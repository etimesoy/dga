import time


def is_lucky_ticket(digits: list[int]) -> bool:
    """
    Проверяет билет на счастливость по казанским правилам
    :param digits: билет в виде массива цифр
    :return: True, если билет счастливый, иначе - False
    """
    n = len(digits)
    for i in range(1 << n):
        left_part = 0
        right_part = 0
        for j in range(n):
            if i & (1 << j) != 0:
                left_part += digits[j]
            else:
                right_part += digits[j]
        if left_part == right_part:
            return True
    return False


def make_digits_list(number: int) -> list[int]:
    """
    Превращает билет из числа в массив цифр длины 6.
    Числа, в которых количество цифр меньше 6, дополнены нулями слева.
    :param number: билет
    :return: билет в виде массива цифр
    """
    digits = [0 for _ in range(6)]
    for i in range(1, 7):
        digits[-i] = number % 10
        number //= 10
    return digits


def main():
    count = 0
    for number in range(1_000_000):
        digits = make_digits_list(number)
        if is_lucky_ticket(digits):
            count += 1
    print(count)  # 376414


if __name__ == "__main__":
    start_time = time.perf_counter()
    main()
    print(f"Время выполнения: {time.perf_counter() - start_time}")

"""
Задача № 2 (5,5 баллов)
Задача про счастливые билеты по казански. Найти количество счастливых билетов среди 6-значных билетов.
"""
