def total_sum(array: list[int]) -> int:
    """
    Находит сумму всех чисел в массиве, возведенных в квадрат
    :param array: массив с числами
    :return: описанная сумма
    """
    return sum(map(lambda x: x ** 2, array))


def get_next_array(array: list[int]) -> list[int]:
    """
    Формирует массив для очередного шага алгоритма на основе предыдущего
    :param array: массив с предыдущего шага
    :return: массив для текущего шага
    """
    next_array = []
    last_ten_sum = 0
    for element in array[:10]:
        last_ten_sum += element
        next_array.append(last_ten_sum)

    for i in range(10, len(array)):
        last_ten_sum -= array[i - 10]
        last_ten_sum += array[i]
        next_array.append(last_ten_sum)

    last_ten_sum -= array[-10]

    for element in array[-9:]:
        next_array.append(last_ten_sum)
        last_ten_sum -= element

    return next_array


def get_lucky_tickets_count(n: int) -> int:
    """
    Находит количество счастливых билетов по-московски
    :param n: длина билетов
    :return: количество билетов
    """
    arr = [1 for _ in range(10)]
    for _ in range(2, n // 2 + 1):
        arr = get_next_array(arr)

    return total_sum(arr)


def main():
    print(get_lucky_tickets_count(6))


if __name__ == "__main__":
    main()

"""
Задача № 1 (7 баллов)
Задача про счастливые билеты по московски. Найти количество счастливых билетов среди 200-значных билетов.
"""
