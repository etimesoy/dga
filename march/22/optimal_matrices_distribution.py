MAX_VALUE = 99999999999
number_of_matrices = int(input("Введите количество матриц: "))
operations_amount = [[MAX_VALUE for _ in range(number_of_matrices + 1)] for _ in range(number_of_matrices + 1)]
indices_of_brackets = [[0 for _ in range(number_of_matrices + 1)] for _ in range(number_of_matrices + 1)]
matrices_dimensions = [0 for _ in range(number_of_matrices + 1)]


def multiply(i: int, j: int) -> int:
    if operations_amount[i][j] == MAX_VALUE:
        for k in range(i, j):
            current_amount = multiply(i, k) + multiply(k + 1, j) + \
                             matrices_dimensions[i - 1] * matrices_dimensions[k] * matrices_dimensions[j]
            if current_amount < operations_amount[i][j]:
                operations_amount[i][j] = current_amount
                indices_of_brackets[i][j] = k
    return operations_amount[i][j]


def pretty_format(i: int, j: int) -> str:
    if i == j == 1:
        return "Matrix1"
    elif i == j:
        return "Matrix" + str(i)
    return "(" + pretty_format(i, indices_of_brackets[i][j]) + " * " + \
           pretty_format(indices_of_brackets[i][j] + 1, j) + ")"


def main():
    for i in range(1, number_of_matrices + 1):
        matrices_dimensions[i - 1], matrices_dimensions[i] = map(int, input().split())
        operations_amount[i][i] = 0
    multiply(1, number_of_matrices)
    print("Ответ:", pretty_format(1, number_of_matrices))


if __name__ == "__main__":
    main()

"""
Для этой задачи есть материал у Кормена в главе 15.2
Баллы: 10
Дополнительные баллы: 4
Дедлайн: 22.03.2023
Задача: Дано N-ное количество матриц, не обязательно квадратных.
 Для каждой i-ой матрицы заданы не значения матрицы, а только размер (ni; mi).
  Эти матрицы перемножаются друг с другом и это гарантируется.
   То есть для каждого i от 1 до 99 действительно то, что m(i) = n (i+1).
    (Здесь в скобочках указаны индексы). От расстановок скобок во время умножения матриц результат не изменится,
      а вот количество поэлементных умножений — изменится.
       Основная задача: определить наименьшее количество поэлементных умножений при наилучшем расставлении скобок.
        Дополнительная задача: вывести расставление скобок, чтобы знать, как надо умножать матрицы.
         Решение — код. Входные данные: число матриц N, и N штук их размеров (ni; mi).

Примеры:
Количество матриц: 3
1 5
5 20
20 1
Ответ: (Matrix1 x (Matrix2 x Matrix3))

"""
