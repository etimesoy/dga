import random


def do_quick_sort_part(array: list[int], left: int, right: int) -> int:
    pivot = array[right]  # по-хорошему выбор pivot должен быть рандомный,
    # но у меня почему-то pivot = array[random.randint(left, right)] не всегда срабатывал
    i = left
    for j in range(left, right):
        if array[j] <= pivot:
            array[i], array[j] = array[j], array[i]
            i += 1
    array[i], array[right] = array[right], array[i]
    return i


def k_th_smallest_element(array: list[int], left: int, right: int, k: int) -> int:
    pivot_index = do_quick_sort_part(array, left, right)

    if k - 1 == pivot_index:
        return array[pivot_index]
    elif k - 1 < pivot_index:
        return k_th_smallest_element(array, left, pivot_index - 1, k)
    else:
        return k_th_smallest_element(array, pivot_index + 1, right, k)


def main():
    array = list(map(int, input("Введите массив чисел, через пробел: ").split()))
    k = int(input("Введите число k: "))
    result = k_th_smallest_element(array, 0, len(array) - 1, k)
    print(f"{k}-ый элемент в отсортированном виде массива: {result}")


if __name__ == "__main__":
    main()

"""
Баллы: 3,5
Дедлайн: 29.03.2023
Задача: Дан массив данных длины n, неотсортированный.
 За O(n) найти k-ый элемент в отсортированном виде этого массива.
"""
