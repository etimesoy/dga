import random


def miller_rabin(n: int, k: int) -> bool:
    # Запишим n-1 как 2^s * d, где d нечетно.
    s, d = 0, n - 1
    while d % 2 == 0:
        s += 1
        d >>= 1  # то же самое, что и //= 2, только быстрее, т.к работает с битами

    for _ in range(k):
        a = random.randint(2, n - 2)
        x = pow(a, d, n)

        # Если х равен 1 или -1, то возможно n простое. Продолжаем проверять
        if x == 1 or x == n - 1:
            continue

        # Проверим каждый xi = x(i-1) ^ 2 % n
        for _ in range(s - 1):
            x = pow(x, 2, n)
            # Если х равен -1, то возможно n простое. Продолжаем проверять
            if x == n - 1:
                break
        else:
            return False

    return True


def main():
    n = int(input("Введите число: "))
    k = int(input("Введите количество проверок: "))
    ans = {
        True: "Простое",
        False: "Составное"
    }
    is_prime = miller_rabin(n, k)
    print(ans[is_prime])


if __name__ == '__main__':
    main()
