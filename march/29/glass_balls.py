"""
Еще не доделал эту задачу. Она не работает сейчас.
"""
import sys

matrix: list[list[int]] = [[]]


def n(d: int, m: int) -> int:
    if d == 0 or m == 0:
        return 0
    try:
        return matrix[d][m]
    except IndexError:
        value = n(d - 1, m - 1) + n(d - 1, m) + 1
        if len(matrix) < d + 1:
            matrix.append([])
        if len(matrix[d]) < m + 1:
            for _ in range(m + 1 - len(matrix[d])):
                matrix[d].append(0)
        matrix[d][m] = value
        return value


def main():
    if len(sys.argv) != 3 or not sys.argv[1].isnumeric() or not sys.argv[2].isnumeric():
        print("Предоставьте два целочисленных позиционных аргумента - N и k")
        exit(1)
    floors_count = int(sys.argv[1])
    balls_count = int(sys.argv[2])
    print(f"Решаю задачу для N = {floors_count} и k = {balls_count}")
    # get_n(floors_count, balls_count)
    # for line in n:
    #     print(line)
    low = 0
    hi = floors_count + 1
    drops = 0
    while n(drops, balls_count) < floors_count:
        drops += 1
    print(f"Потребуется {drops} бросков")
    while low < hi - 1:
        f = low + n(drops - 1, balls_count - 1) + 1
        if f > floors_count:
            f = floors_count
        user_input = int(input(f"Киньте с {f} этажа. Если разбился, введите 1. Если нет, то 0: "))
        if user_input == 1:
            hi = f
            floors_count = balls_count - 1
        else:
            low = f
        drops -= 1
    print(f"Начинают биться с {low} этажа")


if __name__ == "__main__":
    main()

"""
Баллы: 10
Дедлайн: 29.03
Задача: Даны N-этажное здание и k стеклянных шаров.
 Начиная с определенного этажа, шары могут разбиться (а могут и не разбиться, если N этажей не достаточно).
  Если один шар разбился, то у Вас осталось на один шар меньше 🙂.
   Определить минимальное количество бросков этих шаров с различных этажей, чтобы гарантированно определить,
    начиная с какого этажа, шары бьются. При этом нужно написать алгоритм выбора этажа на каждом броске.
     Решение — код. Входные данные: N и k, при k > 2.
      Код должен работать на числах, равных гуголу (единица и 100 нулей).

Пример: 
Входные данные: N = 5, k = 2 (в задаче k должно быть больше 2)
Ответ алгоритма: Потребуется 3 броска, начните с 3го этажа
Ваше сообщение: Разбился
Ответ алгоритма: Киньте с 1го этажа
Ваше сообщение: Не разбился
Ответ алгоритма: Киньте со 2го этажа
Ваше сообщение: Не разбился
Ответ алгоритма: Начинают биться с 3го этажа.
"""
