import sys


matrix: list[list[int]] = []
common_subsequence: list[str] = []


def form_sequence(index1: int, index2: int, text: str):
    if index1 == 0 or index2 == 0:
        return
    if matrix[index1][index2] == matrix[index1 - 1][index2]:
        form_sequence(index1 - 1, index2, text)
    elif matrix[index1][index2] == matrix[index1][index2 - 1]:
        form_sequence(index1, index2 - 1, text)
    elif matrix[index1][index2] == matrix[index1 - 1][index2 - 1] + 1:
        global common_subsequence
        common_subsequence.append(text[index1 - 1])
        form_sequence(index1 - 1, index2 - 1, text)


def algorithm(text1: str, text2: str) -> str:
    text1_len = len(text1)
    text2_len = len(text2)
    global matrix
    matrix = [[0] * (text2_len + 1) for _ in range(text1_len + 1)]

    for i in range(text1_len + 1):
        for j in range(text2_len + 1):
            if i == 0 or j == 0:
                result = 0
            elif text1[i - 1] == text2[j - 1]:
                result = matrix[i - 1][j - 1] + 1
            else:
                result = max(matrix[i - 1][j], matrix[i][j - 1])
            matrix[i][j] = result

    for line in matrix:
        print(line)

    common_subsequence_length = matrix[text1_len][text2_len]
    global common_subsequence
    common_subsequence = [""] * (common_subsequence_length + 1)

    form_sequence(len(text1), len(text2), text1)

    return "".join(reversed(common_subsequence))


def main():
    if len(sys.argv) != 3:
        print("Предоставьте два позиционных аргумента - две последовательности")
        exit(1)
    text1 = sys.argv[1]
    text2 = sys.argv[2]
    result = algorithm(text1, text2)
    print(len(result), result)


if __name__ == "__main__":
    main()

"""
Баллы: 5
Дедлайн: 15.03
Задача: Даны 2 последовательности. Нужно найти Наибольшую Общую Подпоследовательность (НОП).
 Нужно вывести длину НОП и саму НОП. Решение — код. Входные данные: 2 последовательности.

Наибольшая Общая Подпоследовательность (НОП) — подпоследовательность максимальной длины,
 которая есть в обеих последовательностях. Элементы подпоследовательности в самих последовательностях
  идут в том же порядке, но необязательно идут подряд.

Пример: 
Входные данные: УНИВЕРСИТЕТ УНИКС
Ответ: 4 УНИС

В примере видно, что У идёт до Н, Н идёт до И, И идёт до С.
 Также УНИС идет в словах не подряд, между И и С есть ВЕР и К, соответственно.
"""
