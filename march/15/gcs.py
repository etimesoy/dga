matrix: list[list[int]] = []


def gcs(lst1: list[int], lst2: list[int]):
    global matrix
    for i in range(1, len(lst1) + 1):
        for j in range(1, len(lst2) + 1):
            if lst1[i - 1] == lst2[j - 1]:
                matrix[i][j] = matrix[i - 1][j - 1] + 1
            else:
                matrix[i][j] = max(matrix[i - 1][j], matrix[i][j - 1])


def main():
    lst = list(map(int, input("Введите последовательность: ").split()))
    sorted_unique_lst = sorted(list(set(lst)))
    print("sorted_unique_lst =", sorted_unique_lst)

    lst_len = len(lst)
    global matrix
    matrix = [[0] * (len(lst) + 1) for _ in range(len(sorted_unique_lst) + 1)]
    gcs(sorted_unique_lst, lst)

    sequence = []
    for line in matrix:
        print(line)
    for i in range(1, len(sorted_unique_lst) + 1):
        if matrix[i - 1][lst_len] != matrix[i][lst_len]:
            sequence.append(sorted_unique_lst[i - 1])

    print("Длина:", matrix[-1][-1])
    print("Последовательность:", sequence)


if __name__ == "__main__":
    main()


"""
Баллы: 3.5
Дедлайн: 15.03
Задача: Дана последовательность. Нужно найти Наибольшую Возрастающую Подпоследовательность (НВП).
 Можно основываться на решении предыдущей задачи. Решение — код. Входные данные: Последовательность.

Наибольшая Возрастающая Подпоследовательность (НВП) — подпоследовательность максимальной длины,
 где элементы идут в таком же, как и в последовательности, но необязательно идут подряд.
  Причём каждый следующий элемент подпоследовательности больше предыдущего.
   Если взять Наибольшую Неубывающую Подпоследовательность, то каждый следующий элемент подпоследовательности
    не меньше предыдущего.

Входные данные: 20 29 21 6 11 10 20 6 21
Ответ: 
4
6 10 20 21

ИЛИ

Ответ: 
4
6 11 20 21
"""
