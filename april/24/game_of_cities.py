from collections import defaultdict


class Graph:
    def __init__(self):
        self.vertices_dict: dict[str, list[str]] = defaultdict(list)
        self.in_degrees: dict[str, int] = defaultdict(int)

    def add_edge(self, vertex: str, neighbor: str):
        self.vertices_dict[vertex].append(neighbor)
        self.in_degrees[neighbor] += 1


def dfs(graph: Graph, vertex: str, visited: set[str]):
    visited.add(vertex)
    for neighbor in graph.vertices_dict[vertex]:
        if neighbor not in visited:
            dfs(graph, neighbor, visited)


def is_single_component(graph: Graph) -> bool:
    visited: set[str] = set()
    for vertex in graph.vertices_dict:
        if len(graph.vertices_dict[vertex]) != 0:
            dfs(graph, vertex, visited)
            break

    for vertex in graph.vertices_dict:
        if vertex not in visited and len(graph.vertices_dict[vertex]) != 0:
            return False
    return True


def has_euler_path(graph: Graph) -> bool:
    out_degree_condition = in_degree_condition = False
    for vertex in graph.vertices_dict:
        out_degree = len(graph.vertices_dict[vertex])
        in_degree = graph.in_degrees[vertex]

        if out_degree != in_degree:
            if out_degree - in_degree == 1 and not out_degree_condition:
                out_degree_condition = True
            elif in_degree - out_degree == 1 and not in_degree_condition:
                in_degree_condition = True
            else:
                return False

    undirected_graph = Graph()
    for vertex in graph.vertices_dict:
        for neighbor in graph.vertices_dict[vertex]:
            undirected_graph.add_edge(vertex, neighbor)
            undirected_graph.add_edge(neighbor, vertex)
    return is_single_component(undirected_graph)


def main():
    graph = Graph()
    n = int(input("Введите количество городов: "))
    for _ in range(n):
        city_name = input()
        if len(city_name) < 2:
            print("Название каждого города должно иметь как минимум 2 символа")
            exit(0)
        first_letter, last_letter = city_name[0], city_name[-1]
        graph.add_edge(first_letter, last_letter)

    if has_euler_path(graph):
        print("В города сыграть можно!")
    else:
        print("Не получится сыграть в города :(")


if __name__ == "__main__":
    main()

"""
Дисклеймер: конкретно за код выше Лернер поставил 6.5 баллов,
 т.к. здесь нет вывода последовательности слов для игры, а только true/false 

Задача об игре в города - 8.5 баллов
Задан список городов (входные данные: первое число - кол-во городов, далее каждый город с новой строки),
 для него определить можно ли, используя каждый город ровно по одному разу,
  сыграть в игру в города (каждый последующий город начинается с буквы, с которой заканчивался предыдущий).

Примеры:
1. 3 города: АБ, БВ, АВ - нельзя
2. 3 города: АБ, БВ, ВА - можно
3. 4 города: АБ, БВ, ВА, БГ - можно
4. 6 городов: АБ, БВ, ВА, БГ, ДГ, ЕГ - нельзя
"""
